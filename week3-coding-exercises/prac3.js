//Practical Week 3 Aliftarisdha M
// To check the code, uncomment the corresponding question

objectToHTML();
question2();
question4();

//Question 1
function objectToHTML(){
    let output = "" //Empty output
    
    //Question 1
    var testObj = {
        number: 1,
        string: "abc",
        array: [5, 4, 3, 2, 1],
        boolean: true,
    };
    
    for (let prop in testObj){
        output += prop + ": " + testObj[prop] + "\n";
    }
    let outPutArea = document.getElementById("outputArea1")
    outPutArea.innerText = output
}

//Question 2
function question2(){
    var outputAreaRef = document.getElementById("outputArea2");
    var output = ""

    function flexible(fOperation, operand1, operand2)
    {
    var result = fOperation(operand1, operand2);
    return result;
    }

    function add(x,y){
    return x + y
    }

    function multiply(x,y){
    return x * y
    }
    
    output += flexible(add, 3, 5);
    output += "\n" + flexible(multiply, 3, 5);
    let outPutArea = document.getElementById("outputArea2")
    outPutArea.innerText = output
}

//Question 3
/** Pseudocode
Create an array containing multiple numbers
Create a function called extremeValues with one parameter 
Define two variables named min and max
Use for loop to check through the values in the array
Use if statement to check if the values smaller/bigger than the next value then push it to the min/max variable
Print the min and max variable to show the result
**/

//Question 4
function question4(){
    let output = ""
    
    var values = [4, 3, 6, 12, 1, 3, 8];
    function extremeValues(){
        let myArray = []
        let min = values[0]
        let max = values[0]
        for(let i = 0; i < values.length; i++){
            if(min < values[i]){
                min = values[i]
            } else if(max > values[i]){
                max = values[i]
            }
        }
        myArray.push(min)
        myArray.push(max)
        return myArray
    }
    output += extremeValues()[0]
    output += "\n" + extremeValues()[1]
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output
}



