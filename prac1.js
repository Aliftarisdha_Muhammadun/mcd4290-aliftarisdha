// Task 1

var radius = 4;
var circumference = 2 * Math.PI * radius;
var ans = circumference.toFixed(2);
console.log(ans);

//Task 2

let animalString = "cameldogcatlizard";
let andString = " and ";
console.log(animalString.substring(8,11) + andString + animalString.substring(5,8));

//Task 3

var dataPerson ={
    firstName : 'Kanye',
    lastName : 'West',
    birthDate : '8 June 1977',
    annualIncome : 150000000
   }
console.log(dataPerson.firstName + " " + dataPerson.lastName + " " + "was born on" + " " + dataPerson.birthDate + " " + "and has an annual income of" + " "+ dataPerson.annualIncome);

//Task 4

var number1, number2;

number1 = Math.floor((Math.random() * 10) + 1);
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);

//Here 
myNumber = number1;
number1 = number2;
number2 = myNumber;
console.log("number1 = " + number1 + " number2 = " + number2);

//Task 5
let year;
let yearNot2015Or2016;


year = 2000;
yearNot2015Or2016 = (year !== 2015) && (year !== 2016);
console.log(yearNot2015Or2016);
