// Week 4 Practical
function doIt() {
    let output = ""
    
    let number1 = Number(document.getElementById("number1").value)
    let number2 = Number(document.getElementById("number2").value)
    let number3 = Number(document.getElementById("number3").value)
    
    let answer = number1 + number2 + number3
    let sign = document.getElementById("answer")
    let oddeven = document.getElementById("oddeven")
    
    if(answer < 0){
        sign.className = "negative"
    } else{
        sign.className = "positive"
    }
    
    if(answer % 2 === 0){
        oddeven.innerHTML = "Even"
        oddeven.className = "even"
    } else {
        oddeven.innerHTML = "Odd"
        oddeven.className = "odd"
    }
    
    document.getElementById("answer").innerHTML = answer
}