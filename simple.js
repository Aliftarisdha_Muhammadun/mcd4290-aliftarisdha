//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    let positiveOdd = [];
    let negativeEven = [];
    for (let i = 0; i < myArray.length; i++){
        if(myArray[i] > 0 && myArray[i]%2 !== 0){
            positiveOdd.push(myArray[i])
        }
        else if(myArray[i] < 0 && myArray[i]%2 === 0){
            negativeEven.push(myArray[i])
        }
    }
    output += "Positive Odd: " + positiveOdd;
    output += "\n Negative Even: " + negativeEven;
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    var frequency1 = 0;
    var frequency2 = 0;
    var frequency3 = 0;
    var frequency4 = 0;
    var frequency5 = 0;
    var frequency6 = 0;
    for (roll = 0; roll <  60000; roll++) {
        var diceValue = Math.floor((Math.random() * 6) + 1);
        if (diceValue === 1){
            frequency1++;
        } else if (diceValue === 2) {
            frequency2++;
        } else if (diceValue === 3) {
            frequency3++;
        } else if (diceValue === 4) {
            frequency4++;
        } else if (diceValue === 5) {
            frequency5++;
        } else if (diceValue === 6) {
            frequency6++;
        } else {
            console.log("Error: Invalid value");
        }
    }
    output += "Frequency of Dice Rolls";
    output += "\n 1: " + frequency1;
    output += "\n 2: " + frequency2;
    output += "\n 3: " + frequency3;
    output += "\n 4: " + frequency4;
    output += "\n 5: " + frequency5;
    output += "\n 6: " + frequency6;
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    var freqArray = [0, 0, 0, 0, 0, 0, 0];
    for (var roll = 0; roll < 60000; roll++) {
        freqArray[Math.floor((Math.random() * 6) + 1)]++;
    }
    output += "Frequency of Dice Rolls";
    for (var i = 1; i <= 6; i++) {
        output += "\n" + i + ": " + freqArray[i];
    }
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    var dieRolls = {
        Frequencies: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
        },
        Total: 60000,
        Exceptions: ""
    };
    for (var roll = 0; roll < dieRolls.Total; roll++){
        var dieValue = Math.floor((Math.random() * 6) + 1);
        dieRolls.Frequencies[dieValue]++;
    }
    var expectedValue = 10000;
    var limitValue = expectedValue * 0.01;
    for (i = 1; i <= 6; i++) {
        var difference = dieRolls.Frequencies[i] - expectedValue;
        if (Math.abs(difference) > limitValue) {
            dieRolls.Exceptions += i + " ";
        }
    }
    output += "Frequency of Dice Rolls";
    output += "\n Total Rolls: " + dieRolls.Total;
    output += "\n Frequencies: ";
    for (var i in dieRolls.Frequencies) {
        output += "\n" + i + ": " + dieRolls.Frequencies[i];
    }
    output += "\n Exceptions: " + dieRolls.Exceptions;
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    let person = {
        Name: "Jane",
        Income: 150000
    }
    let currentIncome = person.Income;
    let tax;
    if (currentIncome < 18200) {
        tax = 0
    } else if (currentIncome > 18200 && currentIncome < 37000) {
        tax = 0.19 * (currentIncome - 18200)
    } else if (currentIncome < 90000) {
        tax = 3572 + (0.325 * (currentIncome - 37000))
    } else if (currentIncome < 180000) {
        tax = 20797 + (0.37 * (currentIncome - 90000))
    } else {
        tax = 54097 + (0.45 * (currentIncome -180000))
    }
    output += person.Name + "'s income is: $" + person.Income + ", and her tax owed is: $" + tax
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}